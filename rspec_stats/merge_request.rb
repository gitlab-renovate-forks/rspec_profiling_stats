# frozen_string_literal: true

# fetches list of merged MRs which have rspec profiling label

require 'json'
require 'net/http'
require 'time'

class MergeRequest
  URL = URI('https://gitlab.com/api/graphql')
  HEADER = { 'Content-Type' => 'application/json' }.freeze
  MAX_PAGE = 50
  QUERY_TPL = <<QUERY
{
  project(fullPath: "gitlab-org/gitlab") {
    mergeRequests(labels: "rspec profiling", state: merged, sort: MERGED_AT_DESC, after: "%s") {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        mergedAt
        webUrl
        title
        author {
          username
        }
      }
    }
  }
}
QUERY

  def self.fetch(since: nil)
    mrs = fetch_merge_requests.map do |hash|
      new(hash)
    end

    if since
      since_time = Time.parse(since)
      mrs.reject! { |mr| Time.parse(mr.day) < since_time }
    end

    mrs
  end

  def self.fetch_merge_requests
    cursor = ''

    MAX_PAGE.times.each_with_object([]) do |_, mrs|
      puts "fetching rspec profiling MRs, cursor: #{cursor}"
      template = QUERY_TPL % cursor
      request_data = { query: template }.to_json
      response = Net::HTTP.post(URL, request_data, HEADER)
      json = JSON.parse(response.body)
      # response = File.read('/tmp/mrs.json')
      # json = JSON.parse(response)

      mrs.concat(json.dig('data', 'project', 'mergeRequests', 'nodes'))

      page_info = json.dig('data', 'project', 'mergeRequests', 'pageInfo')
      break mrs unless page_info['hasNextPage']

      cursor = page_info['endCursor']
    end
  end

  def initialize(params)
    @params = params
  end

  def day
    @day ||= @params['mergedAt'].sub(/T.*/, '')
  end
end
